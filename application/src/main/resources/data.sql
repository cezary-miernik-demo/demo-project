DROP TABLE IF EXISTS number;

CREATE TABLE number (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  number INT NOT NULL
);

INSERT INTO number (number) VALUES
  (11),
  (23),
  (31),
  (34),
  (45),
  (76),
  (667),
  (67),
  (167),
  (8);
