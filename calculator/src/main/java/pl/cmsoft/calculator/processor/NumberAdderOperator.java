package pl.cmsoft.calculator.processor;

import java.util.Objects;
import java.util.stream.Stream;

class NumberAdderOperator implements Operator<Number> {

    public static final String ARGUMENTS_ARE_NULL = "Arguments are null";

    @Override
    public Number doOperation(Number... numbers) {
        Objects.requireNonNull(numbers, ARGUMENTS_ARE_NULL);
        return Stream.of(numbers)
                .filter(Objects::nonNull)
                .map(Number::doubleValue)
                .reduce(0d, Double::sum);
    }

}
