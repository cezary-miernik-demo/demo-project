package pl.cmsoft.calculator.processor;

import java.util.stream.Stream;

class AdderOperatorFactory implements OperatorFactory {

    @Override
    public Operator create(Object... args) {
        if (isNumericArray(args)) {
            return new NumberAdderOperator();
        }

        throw new IllegalArgumentException(String.format("Adder for passed arguments %s not exist", args));
    }

    private boolean isNumericArray(Object[] args) {
        return Stream.of(args).allMatch(arg -> arg instanceof Number);
    }
}
