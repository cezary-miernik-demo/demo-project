package pl.cmsoft.calculator.processor;

import java.util.Objects;

abstract class OperatorAbstractFactory {

    public static final String OPERATION_IS_UNSUPPORTED = "Operation %s is unsupported.";
    public static final String OPERATION_IS_NULL = "Operation is null";

    public static OperatorFactory getFactory(Operation operation) {
        Objects.requireNonNull(operation, OPERATION_IS_NULL);

        if (Operation.SUM.equals(operation)) {
            return new AdderOperatorFactory();
        }

        throw new IllegalArgumentException(String.format(OPERATION_IS_UNSUPPORTED, operation));
    }

}
