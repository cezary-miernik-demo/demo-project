package pl.cmsoft.calculator.processor;

interface Operator<T> {
    T doOperation(T... args);
}
