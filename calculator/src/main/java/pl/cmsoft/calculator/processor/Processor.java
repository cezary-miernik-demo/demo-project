package pl.cmsoft.calculator.processor;

public interface Processor<T> {
    T process(Operation operation, T... args);
}
