package pl.cmsoft.calculator.processor;

interface OperatorFactory {
    Operator create(Object... args);
}
