package pl.cmsoft.calculator.processor;

import lombok.extern.slf4j.Slf4j;

@Slf4j
class OperationProcessor<T> implements Processor<T> {

    public T process(Operation operation, T... args) {
        OperatorFactory operatorFactory = OperatorAbstractFactory.getFactory(operation);
        Operator<T> operator = operatorFactory.create(args);

        return operator.doOperation(args);
    }

}
