package pl.cmsoft.calculator.processor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class ProcessorConfig {
    @Bean
    public Processor processor() {
        return new OperationProcessor();
    }

}
