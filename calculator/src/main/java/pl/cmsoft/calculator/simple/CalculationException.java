package pl.cmsoft.calculator.simple;

public class CalculationException extends RuntimeException {
    public CalculationException(String message) {
        super(message);
    }
}
