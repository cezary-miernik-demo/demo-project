package pl.cmsoft.calculator.simple;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.cmsoft.calculator.Calculator;
import pl.cmsoft.calculator.processor.Processor;
import pl.cmsoft.datasource.DataSource;

@Configuration
@RequiredArgsConstructor
class SimpleCalculatorConfig {

    private final DataSource randomOrgDataSource;
    private final DataSource javaClassDataSource;
    private final DataSource h2DataSource;
    private final Processor<Number> processor;

    @Bean
    public Calculator calculator() {
        return new SimpleCalculator(processor, randomOrgDataSource, javaClassDataSource, h2DataSource);
    }
}
