package pl.cmsoft.calculator.simple;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.cmsoft.calculator.Calculator;
import pl.cmsoft.calculator.processor.Operation;
import pl.cmsoft.calculator.processor.Processor;
import pl.cmsoft.datasource.DataSource;

import java.util.Optional;
import java.util.function.Supplier;

@RequiredArgsConstructor
@Slf4j
class SimpleCalculator implements Calculator {
    private static final String ADDING_PROCESS_EXCEPTION_MESSAGE = "Adding process with parameters [%f, %f, %f] fails";
    private final Processor<Number> processor;
    private final DataSource randomOrgDataSource;
    private final DataSource javaClassDataSource;
    private final DataSource h2DataSource;

    public Double sumNumbersFromDataSources() {
        Number arg1 = randomOrgDataSource.getNumber();
        Number arg2 = javaClassDataSource.getNumber();
        Number arg3 = h2DataSource.getNumber();

        Number sum = processor.process(Operation.SUM, arg1, arg2, arg3);

        log.debug("Process SUM with params {} {} {} return sum: {}",
                arg1.doubleValue(),
                arg2.doubleValue(),
                arg3.doubleValue(),
                sum);

        return Optional.ofNullable(sum)
                .map(Number::doubleValue)
                .orElseThrow(calculationException(arg1, arg2, arg3));
    }

    private Supplier<CalculationException> calculationException(Number arg1, Number arg2, Number arg3) {
        return () -> new CalculationException(String.format(ADDING_PROCESS_EXCEPTION_MESSAGE, arg1, arg2, arg3));
    }
}
