package pl.cmsoft.calculator.processor;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class OperatorAbstractFactoryTest {

    @Test
    public void shouldReturnAdderFactoryWhenOperationIsSum() {
        //given
        //when
        OperatorFactory operatorFactory = OperatorAbstractFactory.getFactory(Operation.SUM);

        //then
        assertThat(operatorFactory).isNotNull();
        assertThat(operatorFactory).isInstanceOf(AdderOperatorFactory.class);
    }

    @Test
    public void shouldThrowNullPointerExceptionWhenOperationIsNull() {
        //given
        //when
        //then
        assertThatThrownBy(() -> OperatorAbstractFactory.getFactory(null))
                .isInstanceOf(NullPointerException.class)
                .hasMessage("Operation is null");
    }

}
