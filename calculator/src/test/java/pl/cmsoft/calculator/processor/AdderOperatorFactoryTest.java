package pl.cmsoft.calculator.processor;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class AdderOperatorFactoryTest {

    public static final String EXPECTED_EXCEPTION_MESSAGE = "Adder for passed arguments %s not exist";

    @Test
    public void shouldReturnNumberAdderWhenArgsAreNumeric() {
        //given
        OperatorFactory operatorFactory = new AdderOperatorFactory();
        //when
        Operator operator = operatorFactory.create(1, 2, 3);
        //then
        assertThat(operator).isNotNull();
        assertThat(operator).isInstanceOf(NumberAdderOperator.class);
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenArgsAreUnsupported() {
        //given
        OperatorFactory operatorFactory = new AdderOperatorFactory();
        String arg = "Unsupported";
        //when //then
        assertThatThrownBy(() -> operatorFactory.create(arg))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage(EXPECTED_EXCEPTION_MESSAGE, arg);
    }
}
