package pl.cmsoft.calculator.processor;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class NumberAdderOperatorTest {

    public static final String ARGUMENTS_ARE_NULL = "Arguments are null";

    @Test
    protected void shouldReturnSumOfAllArgs() {
        //given
        NumberAdderOperator numberAdderOperator = new NumberAdderOperator();
        Number expectedSum = 6;

        //when
        Number sum = numberAdderOperator.doOperation(1, 2, 3);

        //then
        assertThat(sum).isNotNull();
        assertThat(sum.doubleValue()).isEqualTo(expectedSum.doubleValue());
    }

    @Test
    protected void shouldReturnZeroWhenArgsArrayIsEmpty() {
        //given
        NumberAdderOperator numberAdderOperator = new NumberAdderOperator();
        Number expectedSum = 0;

        //when
        Number sum = numberAdderOperator.doOperation(new Number[]{});

        //then
        assertThat(sum).isNotNull();
        assertThat(sum.doubleValue()).isEqualTo(expectedSum.doubleValue());
    }

    @Test
    protected void shouldThrowNullPointerExceptionWhenArgsArrayIsNull() {
        //given
        NumberAdderOperator numberAdderOperator = new NumberAdderOperator();

        //when //then
        assertThatThrownBy(() -> numberAdderOperator.doOperation(null)).hasMessage(ARGUMENTS_ARE_NULL);
    }
}
