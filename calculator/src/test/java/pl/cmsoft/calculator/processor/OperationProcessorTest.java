package pl.cmsoft.calculator.processor;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class OperationProcessorTest {

    @Test
    public void shouldProcessSumOperation() {
        //given
        Processor<Number> processor = new OperationProcessor<>();
        //when
        Number num = processor.process(Operation.SUM, 1, 1, 1, 1);
        //then
        Assertions.assertThat(num).isNotNull();
        Assertions.assertThat(num).isEqualTo(4.0);

    }

}
