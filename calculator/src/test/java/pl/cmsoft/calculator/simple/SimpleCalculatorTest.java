package pl.cmsoft.calculator.simple;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.cmsoft.calculator.Calculator;
import pl.cmsoft.calculator.processor.Operation;
import pl.cmsoft.calculator.processor.Processor;
import pl.cmsoft.datasource.DataSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

class SimpleCalculatorTest {

    private static final Double NUMBER_FROM_FIST_SOURCE = 1d;
    private static final Double NUMBER_FROM_SECOND_SOURCE = 2d;
    private static final Double NUMBER_FROM_THIRD_SOURCE = 3d;
    private static final Double SUM_OF_NUMBER = NUMBER_FROM_FIST_SOURCE + NUMBER_FROM_SECOND_SOURCE + NUMBER_FROM_THIRD_SOURCE;

    private static Processor<Number> numberProcessor;
    private static DataSource firstDataSource;
    private static DataSource secondDataSource;
    private static DataSource thirdDataSource;

    @BeforeAll
    public static void inti() {
        numberProcessor = spy(Processor.class);
        firstDataSource = spy(DataSource.class);
        secondDataSource = spy(DataSource.class);
        thirdDataSource = spy(DataSource.class);
    }

    @Test
    public void shouldReturnSumOfThreeRandomNumber() {
        //given
        when(firstDataSource.getNumber()).thenReturn(NUMBER_FROM_FIST_SOURCE);
        when(secondDataSource.getNumber()).thenReturn(NUMBER_FROM_SECOND_SOURCE);
        when(thirdDataSource.getNumber()).thenReturn(NUMBER_FROM_THIRD_SOURCE);
        when(numberProcessor.process(Operation.SUM, NUMBER_FROM_FIST_SOURCE, NUMBER_FROM_SECOND_SOURCE, NUMBER_FROM_THIRD_SOURCE))
                .thenReturn(SUM_OF_NUMBER);

        Calculator calculator = new SimpleCalculator(numberProcessor, firstDataSource, secondDataSource, thirdDataSource);
        //when

        Double sum = calculator.sumNumbersFromDataSources();

        //then
        assertThat(sum).isNotNull();
        assertThat(sum).isEqualTo(SUM_OF_NUMBER);
        verify(numberProcessor, times(1)).process(Operation.SUM, NUMBER_FROM_FIST_SOURCE, NUMBER_FROM_SECOND_SOURCE, NUMBER_FROM_THIRD_SOURCE);
        verify(firstDataSource, times(1)).getNumber();
        verify(secondDataSource, times(1)).getNumber();
        verify(thirdDataSource, times(1)).getNumber();
    }

    @Test
    public void shouldThrowCalculationExceptionWhenProcessorReturnNull() {
        //given
        when(firstDataSource.getNumber()).thenReturn(NUMBER_FROM_FIST_SOURCE);
        when(secondDataSource.getNumber()).thenReturn(NUMBER_FROM_SECOND_SOURCE);
        when(thirdDataSource.getNumber()).thenReturn(NUMBER_FROM_THIRD_SOURCE);
        when(numberProcessor.process(Operation.SUM, NUMBER_FROM_FIST_SOURCE, NUMBER_FROM_SECOND_SOURCE, NUMBER_FROM_THIRD_SOURCE))
                .thenReturn(null);

        Calculator calculator = new SimpleCalculator(numberProcessor, firstDataSource, secondDataSource, thirdDataSource);
        //when //then

        assertThatThrownBy(calculator::sumNumbersFromDataSources).isInstanceOf(CalculationException.class)
                .hasMessage("Adding process with parameters [%f, %f, %f] fails", NUMBER_FROM_FIST_SOURCE, NUMBER_FROM_SECOND_SOURCE, NUMBER_FROM_THIRD_SOURCE);

    }
}
