package pl.cmsoft.webservice;

import org.junit.jupiter.api.Test;
import pl.cmsoft.calculator.Calculator;

import static org.mockito.Mockito.*;

class OperationControllerTest {
    private final Calculator calculator = spy(Calculator.class);

    @Test
    public void shouldInvokeCalculatorMethodWhenSumIsCalled() {
        //given
        OperationController operationController = new OperationController(calculator);

        //when
        operationController.sum();
        //then
        verify(calculator, times(1)).sumNumbersFromDataSources();
    }
}
