package pl.cmsoft.webservice;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.cmsoft.calculator.Calculator;

@RestController
@RequestMapping("/operations")
@RequiredArgsConstructor
class OperationController {
    private final Calculator calculator;

    @GetMapping("/sum")
    public Double sum() {
        return calculator.sumNumbersFromDataSources();
    }
}
