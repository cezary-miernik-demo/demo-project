package pl.cmsoft.webservice;


import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@Slf4j
@RestControllerAdvice
class ExceptionHandlerController {

    @ExceptionHandler(value
            = {Exception.class})
    protected ResponseEntity<Object> handleConflict(
            RuntimeException ex, WebRequest request) {

        log.error("Something has gone wrong.", ex);
        String bodyOfResponse = "Example handled exception message";
        return new ResponseEntity<>(bodyOfResponse, HttpStatus.I_AM_A_TEAPOT);
    }

}
