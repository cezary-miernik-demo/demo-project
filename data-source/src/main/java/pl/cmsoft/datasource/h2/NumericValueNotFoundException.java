package pl.cmsoft.datasource.h2;

class NumericValueNotFoundException extends RuntimeException {
    public NumericValueNotFoundException(String message) {
        super(message);
    }
}
