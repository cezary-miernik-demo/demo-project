package pl.cmsoft.datasource.h2;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.cmsoft.datasource.DataSource;

@Configuration
@RequiredArgsConstructor
class H2DataSourceConfig {

    private final NumberRepository numberRepository;

    @Bean
    public DataSource h2DataSource() {
        return new H2DataSource(numberRepository);
    }
}
