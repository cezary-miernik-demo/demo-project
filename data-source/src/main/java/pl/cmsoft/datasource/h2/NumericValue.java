package pl.cmsoft.datasource.h2;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "number")
class NumericValue {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private int number;
}

