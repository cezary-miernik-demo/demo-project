package pl.cmsoft.datasource.h2;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
interface NumberRepository extends CrudRepository<NumericValue, Integer> {
}