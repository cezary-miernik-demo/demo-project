package pl.cmsoft.datasource.h2;

import lombok.RequiredArgsConstructor;
import pl.cmsoft.datasource.DataSource;

import java.util.function.Supplier;

import static pl.cmsoft.datasource.generator.RandomNumberGenerator.generateRandomNumberBetween;

@RequiredArgsConstructor
class H2DataSource implements DataSource {

    public static final String NOT_FOUND_NUMERIC_VALUE_MESSAGE = "Not found value for id : %d";
    private final NumberRepository numberRepository;

    public Number getNumber() {
        Double id = generateRandomNumberBetween(1, 10);
        NumericValue numericValue = numberRepository.findById(id.intValue())
                .orElseThrow(numericValueNotFoundException(id));
        return numericValue.getNumber();
    }

    private Supplier<NumericValueNotFoundException> numericValueNotFoundException(Double id) {
        return () -> new NumericValueNotFoundException(String.format(NOT_FOUND_NUMERIC_VALUE_MESSAGE, id.intValue()));
    }
}
