package pl.cmsoft.datasource.generator;


public abstract class RandomNumberGenerator {
    public static double generateRandomNumberBetween(int startRange, int endRange) {
        return Math.random() * endRange + startRange;
    }
}
