package pl.cmsoft.datasource.javaclass;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.cmsoft.datasource.DataSource;

@Configuration
class JavaClassDataSourceConfig {

    @Bean
    public DataSource javaClassDataSource() {
        return new JavaClassDataSource();
    }
}
