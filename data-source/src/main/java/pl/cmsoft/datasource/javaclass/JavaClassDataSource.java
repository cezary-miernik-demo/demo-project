package pl.cmsoft.datasource.javaclass;

import pl.cmsoft.datasource.DataSource;

import static pl.cmsoft.datasource.generator.RandomNumberGenerator.generateRandomNumberBetween;

class JavaClassDataSource implements DataSource {
    public Number getNumber() {
        return generateRandomNumberBetween(1, 10);
    }
}
