package pl.cmsoft.datasource.randomorg;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import pl.cmsoft.datasource.DataSource;

import java.net.URI;
import java.util.Optional;
import java.util.function.Supplier;

@RequiredArgsConstructor
class RandomOrgDataSource implements DataSource {

    public static final String EXTERNAL_SERVICE_RETURN_INVALID_VALUE_MESSAGE = "External service [%s] does not return a value";
    private final URI uri;
    private final RestTemplate restTemplate;

    public Number getNumber() {
        ResponseEntity<String> response = restTemplate.getForEntity(uri, String.class);
        return Optional.ofNullable(response)
                .map(ResponseEntity::getBody)
                .map(String::trim)
                .filter(NumberUtils::isNumber)
                .map(NumberUtils::createInteger)
                .orElseThrow(externalServiceException());

    }

    private Supplier<ExternalServiceException> externalServiceException() {
        return () -> new ExternalServiceException(String.format(EXTERNAL_SERVICE_RETURN_INVALID_VALUE_MESSAGE, uri));
    }
}
