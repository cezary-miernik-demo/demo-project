package pl.cmsoft.datasource.randomorg;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import pl.cmsoft.datasource.DataSource;

import java.net.URI;

@Configuration
class RandomOrgDataSourceConfig {

    private final URI randomOrgUri;

    RandomOrgDataSourceConfig(@Value("${demo.random-org.uri}") URI randomOrgUri) {
        this.randomOrgUri = randomOrgUri;
    }

    @Bean
    public DataSource randomOrgDataSource() {
        return new RandomOrgDataSource(randomOrgUri, restTemplate());
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}
