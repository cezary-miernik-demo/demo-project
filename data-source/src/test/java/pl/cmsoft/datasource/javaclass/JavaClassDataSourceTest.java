package pl.cmsoft.datasource.javaclass;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.cmsoft.datasource.DataSource;

import static org.assertj.core.api.Assertions.assertThat;

class JavaClassDataSourceTest {

    public static final int EXPECTED_NUMBER = 99;
    public static final int NUMERIC_VALUE_ID = 1;
    public static final int START_RANGE = 1;
    public static final int END_RANGE = 10;

    private static DataSource javaClassDataSource;

    @BeforeAll
    public static void init() {
        javaClassDataSource = new JavaClassDataSource();
    }

    @Test
    public void shouldReturnRandomNumberBetween1And10() {
        //given

        //when
        Number number = javaClassDataSource.getNumber();

        //then
        assertThat(number).isNotNull();
        assertThat(number.intValue()).isBetween(START_RANGE, END_RANGE);
    }


}
