package pl.cmsoft.datasource.h2;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import pl.cmsoft.datasource.DataSource;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class H2DataSourceTest {

    public static final int EXPECTED_NUMBER = 99;
    public static final int NUMERIC_VALUE_ID = 1;

    private static NumberRepository numberRepository;
    private static DataSource h2DataSource;

    @BeforeAll
    public static void init() {
        numberRepository = mock(NumberRepository.class);
        h2DataSource = new H2DataSource(numberRepository);
    }

    @Test
    public void shouldReturnNumber() {
        //given
        Optional<NumericValue> numericValue = prepareNumericValueOptional();
        when(numberRepository.findById(ArgumentMatchers.anyInt())).thenReturn(numericValue);

        //when
        Number number = h2DataSource.getNumber();

        //then
        assertThat(number).isNotNull();
        assertThat(number.intValue()).isEqualTo(EXPECTED_NUMBER);
    }

    @Test
    public void shouldThrowNumericValueNotFoundException() {
        //given
        when(numberRepository.findById(ArgumentMatchers.anyInt())).thenReturn(Optional.empty());

        //then //when
        assertThatThrownBy(h2DataSource::getNumber)
                .isInstanceOf(NumericValueNotFoundException.class)
                .hasMessageContaining("Not found value for id :");
    }

    private Optional<NumericValue> prepareNumericValueOptional() {
        NumericValue numericValue = new NumericValue();
        numericValue.setId(NUMERIC_VALUE_ID);
        numericValue.setNumber(EXPECTED_NUMBER);
        return Optional.of(numericValue);
    }
}
