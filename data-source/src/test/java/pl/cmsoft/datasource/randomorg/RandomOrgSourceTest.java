package pl.cmsoft.datasource.randomorg;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import pl.cmsoft.datasource.DataSource;

import java.net.URI;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class RandomOrgSourceTest {

    public static final URI someUri = URI.create("http://someaddress.com");
    public static final int EXPECTED_VALUE = 99;

    private static RestTemplate restTemplate;
    private static DataSource randomOrgDataSource;

    @BeforeAll
    public static void init() {
        restTemplate = mock(RestTemplate.class);
        randomOrgDataSource = new RandomOrgDataSource(someUri, restTemplate);
    }

    @Test
    public void shouldReturnRandomNumber() {
        //given
        ResponseEntity<String> mockResponse = new ResponseEntity<>(String.valueOf(EXPECTED_VALUE), HttpStatus.OK);
        when(restTemplate.getForEntity(someUri, String.class)).thenReturn(mockResponse);

        //when
        Number number = randomOrgDataSource.getNumber();

        //then
        assertThat(number).isNotNull();
        assertThat(number).isEqualTo(EXPECTED_VALUE);
    }

    @Test
    public void shouldThrowNumericValueNotFoundExceptionWhenServiceReturnNull() {
        //given
        when(restTemplate.getForEntity(someUri, String.class)).thenReturn(null);

        //then //when
        assertThatThrownBy(randomOrgDataSource::getNumber)
                .isInstanceOf(ExternalServiceException.class)
                .hasMessage("External service [%s] does not return a value", someUri);
    }

    @Test
    public void shouldThrowNumericValueNotFoundExceptionWhenServiceReturnEmptyBody() {
        //given
        ResponseEntity<String> mockResponse = new ResponseEntity<>(StringUtils.EMPTY, HttpStatus.OK);
        when(restTemplate.getForEntity(someUri, String.class)).thenReturn(mockResponse);

        //then //when
        assertThatThrownBy(randomOrgDataSource::getNumber)
                .isInstanceOf(ExternalServiceException.class)
                .hasMessage("External service [%s] does not return a value", someUri);
    }

}
