## Demo Project
To run application:
1. Clone repo or download project.
2. In root directory named "demo-project" call **"mvn clean install"** 
3. When application built successfully and all test pass, you have to go into application module using **"cd application"** command.
4. Now you can run application by **"mvn spring-boot:run"**

By default, application starts on https://localhost:8443 address.

To test it you need to send GET request to endpoint https://localhost:8443/operations/sum
